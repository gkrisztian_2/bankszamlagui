﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFBank
{
    class Bank
    {

        public void szamlaletrehoz(int szamlaszam, string tulajdonos, int egyenleg)
        {
            Variables.szamlak.Add(new Szamla(szamlaszam, tulajdonos, egyenleg));
        }

        public void szamlaletrehoz(string tulajdonos)
        {
            int aktszam = Variables.szamlak.Count;
            Variables.szamlak.Add(new Szamla(aktszam, tulajdonos, 1000));
        }
        public void atutal(int honnan, int hova, int osszeg)
        {
            Variables.szamlak[honnan].kivesz(osszeg);
            Variables.szamlak[hova].betesz(osszeg);
        }

        public void listaz() //Feladat a 0. elem kihagyása listázás során
        {
            
            foreach (Szamla elem in Variables.szamlak)
            {
                if(elem.tulajdonos != "" && elem != null)
                    Console.WriteLine(elem.szamlaszam + " " +elem.tulajdonos + " " + elem.lekerdez());
            }
        }
        // betesz és kivesz method!
        
        public void Transaction(ETransaction type, int account, int amount)
        {
            switch (type)
            {
                case ETransaction.Withdraw:
                    {

                        Variables.szamlak[account].kivesz(amount);
                        
                        break;
                    }
                case ETransaction.Deposit:
                    {
                        Variables.szamlak[account].betesz(amount);
                        break;
                    }
            }
            Log.Create("transaction.log", Encoding.UTF8, string.Format($"({Enum.GetName(typeof(ETransaction), (int)type)}) account: {account} amount: {amount}"));
        }

    }
}
