﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFBank
{
    public partial class UC_MenuItem : UserControl
    {
        Color _backColor = Color.FromArgb(233, 234, 236);
        Color _hoverColor = Color.FromArgb(4, 94, 251);
        public bool isItemActive = false;

        public UC_MenuItem()
        {
            InitializeComponent();
            GroupControlls(this);
        }
        private void GroupControlls(Control c)
        {
            foreach (Control item in c.Controls)
            {
                item.Click += ctl_Click;
                if (item.HasChildren)
                {
                    GroupControlls(item);
                }
            }
        }
        private void ctl_Click(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, EventArgs.Empty);
        }
        public bool ItemActive
        {
            get { return isItemActive; }
            set
            {
                isItemActive = value;
            }
        }

        public Image ItemIcon
        {
            get
            {
                return pictureBox1.Image;
            }
            set
            {
                pictureBox1.Image = value;
            }
        }
        public string ItemLabel
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public string ItemDescription
        {
            get { return label2.Text; }
            set { label2.Text = value; }
        }
        private void mEnter(object s, EventArgs e)
        {
            
            this.BackColor = _hoverColor;
            label1.ForeColor = Color.White;
            label2.ForeColor = Color.White;
        }
        private void mLeave(object s, EventArgs e)
        {
            this.BackColor = _backColor;
            label1.ForeColor = Color.Black;
            label2.ForeColor = Color.Black;
        }
        private void UC_MenuItem_MouseEnter(object sender, EventArgs e)
        {
            mEnter(sender, e);
        }

        private void UC_MenuItem_MouseLeave(object sender, EventArgs e)
        {
            mLeave(sender, e);
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            mEnter(sender, e);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            mLeave(sender, e);
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            mEnter(sender, e);
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            mLeave(sender, e);
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            mEnter(sender, e);
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            mLeave(sender, e);
        }
    }
}
