﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFBank
{
    public partial class UC_Bankszamlak : UserControl
    {
        public UC_Bankszamlak()
        {
            InitializeComponent();
            this.OnClick(EventArgs.Empty);
        }

        public ListBox lbSzamlak
        {
            get { return lb_szamlak; }
            set => lbSzamlak = value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bank b = new Bank();
            b.szamlaletrehoz(tb_szamla.Text);
            
            lb_szamlak.Items.Add(tb_szamla.Text);
        }
    }
}
