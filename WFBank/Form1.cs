﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFBank
{
    public partial class Form1 : Form
    {
        Bank bank;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Variables._bank = new Bank();
            Variables._bank.szamlaletrehoz("asd");

            RefreshSzamla();
        }

        private void listaz()
        {
            listBox1.Items.Clear();
            foreach (Szamla elem in Variables.szamlak)
            {
                if (elem.szamlaszam != 0)
                {
                    //Console.WriteLine(elem.szamlaszam + " " + elem.tulajdonos + " " + elem.lekerdez());
                    listBox1.Items.Add(elem.szamlaszam + " " + elem.tulajdonos + " " + elem.lekerdez());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Variables._bank.szamlaletrehoz(textBox1.Text);
            listaz();
            textBox1.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Variables.szamlak[listBox1.SelectedIndex + 1].betesz(Convert.ToInt32(textBox2.Text));
            listaz();
            textBox2.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Variables.szamlak[listBox1.SelectedIndex + 1].kivesz(Convert.ToInt32(textBox3.Text));
            listaz();
            textBox3.Clear();
        }

        private void uC_MenuItem1_Click(object sender, EventArgs e)
        {
            RefreshSzamla();
            uC_Bankszamlak1.BringToFront();
            lb_CurrentPage.Text = "Áttekintés";

        }

        private void uC_MenuItem4_Click(object sender, EventArgs e)
        {
            RefreshSzamla();
            uC_Atutalas1.BringToFront();
            lb_CurrentPage.Text = "Átutalás";
            
        }
        void RefreshSzamla()
        {
            uC_Atutalas1.forras.Items.Clear();
            uC_Atutalas1.cel.Items.Clear();
            uC_Bankszamlak1.lbSzamlak.Items.Clear();
            string s = "";
            foreach (var item in Variables.szamlak)
            {
                s = "Szám: " + item.szamlaszam + " Tulajdonos: " + item.tulajdonos + " Egyenleg: " + item.lekerdez();
                uC_Bankszamlak1.lbSzamlak.Items.Add(s);
                uC_Atutalas1.forras.Items.Add(s);
                uC_Atutalas1.cel.Items.Add(s);
            }
            
        }

    }
    
}
