﻿namespace WFBank
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lb_CurrentPage = new System.Windows.Forms.Label();
            this.uC_MenuItem4 = new WFBank.UC_MenuItem();
            this.uC_MenuItem3 = new WFBank.UC_MenuItem();
            this.uC_MenuItem2 = new WFBank.UC_MenuItem();
            this.uC_MenuItem1 = new WFBank.UC_MenuItem();
            this.uC_Bankszamlak1 = new WFBank.UC_Bankszamlak();
            this.uC_Atutalas1 = new WFBank.UC_Atutalas();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(246, 176);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 212);
            this.listBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(407, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Létrehoz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(493, 187);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(148, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(407, 224);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Betesz";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(407, 266);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Kivesz";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(493, 224);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(493, 268);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(234)))), ((int)(((byte)(236)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.uC_MenuItem4);
            this.panel1.Controls.Add(this.uC_MenuItem3);
            this.panel1.Controls.Add(this.uC_MenuItem2);
            this.panel1.Controls.Add(this.uC_MenuItem1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(212, 480);
            this.panel1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(46, 458);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "2021 - Géczy Krisztián";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "BankszámlaGUI";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lb_CurrentPage);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Location = new System.Drawing.Point(212, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(683, 52);
            this.panel2.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(683, 422);
            this.panel3.TabIndex = 9;
            // 
            // lb_CurrentPage
            // 
            this.lb_CurrentPage.AutoSize = true;
            this.lb_CurrentPage.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_CurrentPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(40)))), ((int)(((byte)(52)))));
            this.lb_CurrentPage.Location = new System.Drawing.Point(18, 15);
            this.lb_CurrentPage.Name = "lb_CurrentPage";
            this.lb_CurrentPage.Size = new System.Drawing.Size(101, 23);
            this.lb_CurrentPage.TabIndex = 0;
            this.lb_CurrentPage.Text = "Áttekintés";
            // 
            // uC_MenuItem4
            // 
            this.uC_MenuItem4.ItemActive = false;
            this.uC_MenuItem4.ItemDescription = "Átutalás bankszámlára";
            this.uC_MenuItem4.ItemIcon = ((System.Drawing.Image)(resources.GetObject("uC_MenuItem4.ItemIcon")));
            this.uC_MenuItem4.ItemLabel = "Átutalás";
            this.uC_MenuItem4.Location = new System.Drawing.Point(3, 258);
            this.uC_MenuItem4.Name = "uC_MenuItem4";
            this.uC_MenuItem4.Size = new System.Drawing.Size(208, 55);
            this.uC_MenuItem4.TabIndex = 4;
            this.uC_MenuItem4.Click += new System.EventHandler(this.uC_MenuItem4_Click);
            // 
            // uC_MenuItem3
            // 
            this.uC_MenuItem3.ItemActive = false;
            this.uC_MenuItem3.ItemDescription = "Egyenleg csökkentése";
            this.uC_MenuItem3.ItemIcon = ((System.Drawing.Image)(resources.GetObject("uC_MenuItem3.ItemIcon")));
            this.uC_MenuItem3.ItemLabel = "Kivesz";
            this.uC_MenuItem3.Location = new System.Drawing.Point(2, 195);
            this.uC_MenuItem3.Name = "uC_MenuItem3";
            this.uC_MenuItem3.Size = new System.Drawing.Size(208, 55);
            this.uC_MenuItem3.TabIndex = 3;
            // 
            // uC_MenuItem2
            // 
            this.uC_MenuItem2.ItemActive = false;
            this.uC_MenuItem2.ItemDescription = "Egyenleg növelése";
            this.uC_MenuItem2.ItemIcon = ((System.Drawing.Image)(resources.GetObject("uC_MenuItem2.ItemIcon")));
            this.uC_MenuItem2.ItemLabel = "Betesz";
            this.uC_MenuItem2.Location = new System.Drawing.Point(3, 133);
            this.uC_MenuItem2.Name = "uC_MenuItem2";
            this.uC_MenuItem2.Size = new System.Drawing.Size(208, 55);
            this.uC_MenuItem2.TabIndex = 2;
            // 
            // uC_MenuItem1
            // 
            this.uC_MenuItem1.ItemActive = false;
            this.uC_MenuItem1.ItemDescription = "Bankfiókok";
            this.uC_MenuItem1.ItemIcon = ((System.Drawing.Image)(resources.GetObject("uC_MenuItem1.ItemIcon")));
            this.uC_MenuItem1.ItemLabel = "Áttekintés";
            this.uC_MenuItem1.Location = new System.Drawing.Point(1, 72);
            this.uC_MenuItem1.Name = "uC_MenuItem1";
            this.uC_MenuItem1.Size = new System.Drawing.Size(208, 55);
            this.uC_MenuItem1.TabIndex = 1;
            this.uC_MenuItem1.Click += new System.EventHandler(this.uC_MenuItem1_Click);
            // 
            // uC_Bankszamlak1
            // 
            this.uC_Bankszamlak1.Location = new System.Drawing.Point(217, 58);
            this.uC_Bankszamlak1.Name = "uC_Bankszamlak1";
            this.uC_Bankszamlak1.Size = new System.Drawing.Size(674, 419);
            this.uC_Bankszamlak1.TabIndex = 9;
            // 
            // uC_Atutalas1
            // 
            this.uC_Atutalas1.Location = new System.Drawing.Point(218, 58);
            this.uC_Atutalas1.Name = "uC_Atutalas1";
            this.uC_Atutalas1.Size = new System.Drawing.Size(674, 419);
            this.uC_Atutalas1.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.ClientSize = new System.Drawing.Size(895, 480);
            this.Controls.Add(this.uC_Atutalas1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.uC_Bankszamlak1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Bank";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private UC_MenuItem uC_MenuItem1;
        private UC_MenuItem uC_MenuItem3;
        private UC_MenuItem uC_MenuItem2;
        private System.Windows.Forms.Label label1;
        private UC_MenuItem uC_MenuItem4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lb_CurrentPage;
        private System.Windows.Forms.Panel panel3;
        private UC_Bankszamlak uC_Bankszamlak1;
        private UC_Atutalas uC_Atutalas1;
    }
}

