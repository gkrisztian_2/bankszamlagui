﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFBank
{
    public enum ETransaction { Withdraw, Deposit };

    
    class ETransactionException : Exception
    {
        public override string Message => "Transaction type may not exist.";
    }
    static class Variables
    {
        public static List<Szamla> szamlak = new List<Szamla>();
        public static Bank _bank;
    }

    static class Log
    {
        public static void Create(string f, Encoding encode, string msg)
        {
            try
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(f, true, encode))
                {
                    sw.WriteLine($"{DateTime.Now} - {msg}");
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }

}
