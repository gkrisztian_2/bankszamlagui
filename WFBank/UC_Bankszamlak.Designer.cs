﻿
namespace WFBank
{
    partial class UC_Bankszamlak
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_szamlak = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_szamla = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lb_szamlak
            // 
            this.lb_szamlak.FormattingEnabled = true;
            this.lb_szamlak.Location = new System.Drawing.Point(43, 123);
            this.lb_szamlak.Name = "lb_szamlak";
            this.lb_szamlak.Size = new System.Drawing.Size(594, 264);
            this.lb_szamlak.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(384, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Létrehozás";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_szamla
            // 
            this.tb_szamla.Location = new System.Drawing.Point(207, 60);
            this.tb_szamla.Name = "tb_szamla";
            this.tb_szamla.Size = new System.Drawing.Size(171, 20);
            this.tb_szamla.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Új bankszámla";
            // 
            // UC_Bankszamlak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_szamla);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lb_szamlak);
            this.Name = "UC_Bankszamlak";
            this.Size = new System.Drawing.Size(674, 419);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_szamlak;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_szamla;
        private System.Windows.Forms.Label label1;
    }
}
