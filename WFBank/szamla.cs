﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFBank
{
    class Szamla
    {
        public int szamlaszam;
        public string tulajdonos;
        private int egyenleg;

        public Szamla(int szamlaszam, string tulajdonos, int egyenleg)
        {
            this.szamlaszam = szamlaszam;
            this.tulajdonos = tulajdonos;
            this.egyenleg = egyenleg;
        }
        public int lekerdez()
        {
            return egyenleg;
        }
        public void betesz(int osszeg)
        {
            egyenleg = egyenleg + osszeg;
            //egyenleg += osszeg;
        }
        public void kivesz(int osszeg)
        {
            egyenleg = egyenleg - osszeg;
            //egyenleg -= osszeg;

        }

    }
}
