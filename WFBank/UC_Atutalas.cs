﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFBank
{
    public partial class UC_Atutalas : UserControl
    {
        public UC_Atutalas()
        {
            InitializeComponent();

            foreach (var item in Variables.szamlak)
            {
                comboBox1.Items.Add("Szám: " + item.szamlaszam + " Tulajdonos: " + item.tulajdonos + " Egyenleg: " + item.lekerdez());
                comboBox2.Items.Add("Szám: " + item.szamlaszam + " Tulajdonos: " + item.tulajdonos + " Egyenleg: " + item.lekerdez());
            }
        }

        public ComboBox forras
        {
            get { return comboBox1; }

            set => comboBox1 = value;
        }
        public ComboBox cel
        {
            get { return comboBox2; }

            set => comboBox2 = value;
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            Variables._bank.atutal(comboBox1.SelectedIndex, comboBox2.SelectedIndex, int.Parse(textBox1.Text));
            MessageBox.Show(Variables.szamlak[comboBox1.SelectedIndex].lekerdez().ToString());
           
        }
    }
}
